package eu.tibai.modulecore;

import eu.tibai.internalmodule.InternalPrinter;
import eu.tibai.externalmodule.ExternalPrinter;

public class Controller {
    public static void main(String[] args){
        new InternalPrinter().print();
        new ExternalPrinter().print();
    }
}
